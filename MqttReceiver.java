import java.io.FileWriter;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Properties;
import org.eclipse.paho.client.mqttv3.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class MqttReceiver {

    public static void main(String[] args) {
        String broker = "tcp://127.0.0.1:1883";
        String topic = "transport/data";
        String clientId = "JavaSubscriber";
        MqttClient client;
        // Kafka configuration
        String kafkaBootstrapServers = "localhost:9092"; // Update with your Kafka bootstrap servers
        Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers", kafkaBootstrapServers);
        kafkaProps.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<String, String> producer = new KafkaProducer<>(kafkaProps);

        try {
            client = new MqttClient(broker, clientId);
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    System.out.println("Connection lost!");
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) {
                    byte[] encryptedData = message.getPayload();

                    // Decrypt the data
                    String decryptedData = decryptData(encryptedData, "Sixteen byte key");
                    if (decryptedData != null) {
                        System.out.println("Decrypted data: " + decryptedData);
                        // Attempt to parse decrypted data
                        String jsonObject = parseDecryptedData(decryptedData);
                        if (jsonObject != null) {
                            System.out.println("Parsed JSON data: " + jsonObject);
                            // Send parsed data to Kafka topic
                            ProducerRecord<String, String> record = new ProducerRecord<>(topic, jsonObject); // Updated
                                                                                                             // to use
                                                                                                             // 'topic'
                                                                                                             // variable
                            producer.send(record);
                        } else {
                            System.out.println("Parsed JSON object is null.");
                        }

                    }
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    // Not used in this example
                }
            });

            client.connect();
            client.subscribe(topic);
        } catch (MqttException e) {
            e.printStackTrace();
        } finally {
            producer.close(); // Close the Kafka producer
        }
    }

    private static String decryptData(byte[] encryptedData, String key) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            // Extract IV from the first 16 bytes of encrypted data
            byte[] iv = new byte[16];
            System.arraycopy(encryptedData, 0, iv, 0, iv.length);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
            // Decrypt the data except for the IV
            byte[] decrypted = cipher.doFinal(encryptedData, 16, encryptedData.length - 16);
            return new String(decrypted, "UTF-8").trim(); // Convert bytes to string
        } catch (Exception e) {
            System.err.println("Error decrypting data: " + e.getMessage());
            return null;
        }
    }

    private static String parseDecryptedData(String decryptedData) {
        try {
            // Split decrypted data into fields
            String[] fields = decryptedData.split(",");
            String incomingChecksum = fields[26].trim().substring(1);
            // Calculate checksum
            String checksum = calculateChecksum(decryptedData);

            if (!checksum.equals(incomingChecksum)) {
                System.out.println("Checksum does not match!");
                try (FileWriter fileWriter = new FileWriter("corrupted_data.csv", true)) {
                    fileWriter.write(decryptedData + "\n");
                } catch (Exception e) {
                    System.err.println("Error writing corrupted data to CSV: " + e.getMessage());
                    e.printStackTrace();
                }
                return null;
            } else {
                // Create JSON string manually
                StringBuilder jsonBuilder = new StringBuilder();
                jsonBuilder.append("{");
                appendField(jsonBuilder, "StartCharacter", "\"" + fields[0].trim() + "\"", false);
                appendField(jsonBuilder, "Header", "\"" + fields[1].trim() + "\"");
                appendField(jsonBuilder, "FirmwareVersion", fields[2].trim());
                appendField(jsonBuilder, "ConfigVersion", fields[3].trim());
                appendField(jsonBuilder, "PacketType", "\"" + fields[4].trim() + "\"");
                appendField(jsonBuilder, "PacketStatus", "\"" + fields[5].trim() + "\"");
                appendField(jsonBuilder, "IMEI", "\"" + fields[6].trim() + "\"");
                appendField(jsonBuilder, "GPSFix", fields[7].trim());
                appendField(jsonBuilder, "Date", "\"" + fields[8].trim() + "\"");
                appendField(jsonBuilder, "Time", "\"" + fields[9].trim() + "\"");
                appendField(jsonBuilder, "Latitude", fields[10].trim());
                appendField(jsonBuilder, "LatitudeDirection", "\"" + fields[11].trim() + "\"");
                appendField(jsonBuilder, "Longitude", fields[12].trim());
                appendField(jsonBuilder, "LongitudeDirection", "\"" + fields[13].trim() + "\"");
                appendField(jsonBuilder, "Speed", fields[14].trim());
                appendField(jsonBuilder, "Heading", fields[15].trim());
                appendField(jsonBuilder, "NoofSatellites", fields[16].trim());
                appendField(jsonBuilder, "Altitude", fields[17].trim());
                appendField(jsonBuilder, "PDOP", fields[18].trim());
                appendField(jsonBuilder, "HDOP", fields[19].trim());
                appendField(jsonBuilder, "NetworkOperatorName", "\"" + fields[20].trim() + "\"");
                appendField(jsonBuilder, "IgnitionStatus", fields[21].trim());
                appendField(jsonBuilder, "MainInputVoltage", fields[22].trim());
                appendField(jsonBuilder, "GSMSignalStrength", fields[23].trim());
                appendField(jsonBuilder, "GPRSStatus", fields[24].trim());
                appendField(jsonBuilder, "FrameNumber", fields[25].trim());
                appendField(jsonBuilder, "EndCharacter", "\"" + fields[26].trim() + "\"", true);
                appendField(jsonBuilder, "Checksum", "\"" + checksum + "\"", true); // Include checksum in JSON
                jsonBuilder.append("}");
                // send jsonBuilder tokafka topic
                return jsonBuilder.toString();
            }
        } catch (Exception e) {
            System.err.println("Failed to parse decrypted data: " + decryptedData);
            e.printStackTrace();
            return null;
        }
    }

    private static String calculateChecksum(String data) {
        // Extract fields excluding StartCharacter and Checksum
        String[] fields = data.split(",");
        String checksum = "test";

        int sum = 0;
        for (int i = 1; i < fields.length - 1; i++) {
            // Iterate over each character in the field and sum their ASCII values
            for (char c : fields[i].toCharArray()) {
                sum += (int) c;
            }
        }
        // Take the modulo 256 of the sum to get the checksum value
        int checksumValue = sum;

        // Using concatenation with an empty string

        checksum = String.valueOf(checksumValue);
        return checksum;
    }

    private static void appendField(StringBuilder jsonBuilder, String key, String value) {
        jsonBuilder.append("\"").append(key).append("\":").append(value).append(",");
    }

    private static void appendField(StringBuilder jsonBuilder, String key, String value, boolean last) {
        jsonBuilder.append("\"").append(key).append("\":").append(value);
        if (!last) {
            jsonBuilder.append(",");
        }
    }
}